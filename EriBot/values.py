ASK_FOR_CONTINUE = "press any key to continue."
SECRET_NUMBER = 9999046
# Files to be printed

LOADING_TIME = 100
L = list(range(4)) # Progress bar colomns

QUESTIONS = [
    {
        'type': 'list',
        'name': 'user_option',
        'message': 'Did you get the correct number?',
        'choices': ["Yes, I got it", "Not yet"]
    }
]

INTERVIEW_SCHEDULE = [
    {
        'name': 'Ruben',
        'start_time': '00:00',
        'end_time': '19:59',
        'day': '26'
    },
    {
        'name': 'Hanna',
        'start_time': '10:50',
        'end_time': '11:40',
        'day': '30'
    },
    {
        'name': 'Martyna',
        'start_time': '11:42',
        'end_time': '13:59',
        'day': '30'
    },
    {
        'name': 'Thea',
        'start_time': '14:00',
        'end_time': '15:30',
        'day': '30'
    },
    {
        'name': 'Alexander',
        'start_time': '00:00',
        'end_time': '19:59',
        'day': '1'
    },
    {
        'name': 'Malte',
        'start_time': '00:00',
        'end_time': '19:59',
        'day': '29'
    },
    {
        'name': 'Jacob',
        'start_time': '00:00',
        'end_time': '19:59',
        'day': '31'
    },


]