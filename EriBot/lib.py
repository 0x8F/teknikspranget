import time
import random
import values as val
import click
from time import sleep
from tqdm import trange, tqdm
from datetime import datetime
from multiprocessing import Pool, RLock, freeze_support


def print_string_slowly(string):
    for l in string:
        click.secho("{}".format(l), fg="green", bold=True, underline=True, nl=False)
        rand_sec = random.uniform(0, 0.1)
        time.sleep(rand_sec)
    click.secho("", fg="cyan", bold=True, underline=True, nl=True)  # new line


def print_file_slowly(file):
    with open(file) as file_to_print:
        for line in file_to_print:
            for l in line:
                # Check if line contains (press any key to continue)
                if line.find(val.ASK_FOR_CONTINUE) > -1:
                    click.pause()
                    break
                else:
                    click.secho("{}".format(l), fg="green", bold=True, underline=True, nl=False)
                    rand_sec = random.uniform(0, 0.1)
                    time.sleep(rand_sec)


def progress_over_iterable_with_colors():
    """
    Demonstrates how a progress bar can be tied to processing of
    an iterable - this time with colorful output.
    """

    # Could be a list, tuple and a whole bunch of other containers
    iterable = range(val.LOADING_TIME)

    fill_char = click.style("#", fg="green")
    empty_char = click.style("-", fg="white", dim=True)
    label_text = "Accessing the Kernel ..."

    with click.progressbar(
            iterable=iterable,
            label=label_text,
            fill_char=fill_char,
            empty_char=empty_char
    ) as items:
        for item in items:
            # Do some processing
            time.sleep(0.023)  # This is really hard work


def ask_to_continue():
    click.echo('Continue? [Y/n] ', nl=False)
    c = click.getchar()
    click.echo()
    if c == 'Y':
        click.echo('We will go on')
    elif c == 'n':
        click.echo('Abort!')
    else:
        click.echo('Invalid input :(')


def confirm_as_variable(string) -> None:
    """Asks the user for confirmation and stores the response in a variable"""

    confirmed = click.confirm(string)
    status = click.style("yes", fg="green") if confirmed else click.style("no", fg="red")
    click.echo(": " + status)
    return confirmed


def hour_to_minutes(hour, minutes):
    time_in_minutes = int(hour)*60 + int(minutes)
    return int(time_in_minutes)

def change_string_in_file(new_string):
    # will you change the ip or mcc value?
    if new_string == val.NODE_IP_ADRS:
        file_path = val.NODE_CONFIG_FILE
        old_string = val.HIDDEN_IP_IN_FILE
    else:
        file_path = val._5G_CORE_CONFIG_FILE
        old_string = val.HIDDEN_MCC_IN_FILE
    with open(file_path, 'r') as file:
        file_data = file.read()
    file_data = file_data.replace(old_string, new_string)
    with open(file_path, 'w') as file:
        file.write(file_data)


def progress_bar():
    freeze_support()  # for Windows support
    tqdm.set_lock(RLock())  # for managing output contention
    p = Pool(initializer=tqdm.set_lock, initargs=(tqdm.get_lock(),))
    p.map(progresser, val.L)


def progresser(n):
    interval = 0.001 / (n + 2)
    total = 200
    text = "#{}, est. {:<04.2}s".format(n, interval * total)
    for _ in trange(total, desc=text, position=n):
        sleep(interval)

def secret_number_found_check():
    secret_number = click.prompt(
        text="Let me see it, please enter the number I have in mind ",
        type=click.STRING)
    secret_number = int(secret_number)
    if secret_number == val.SECRET_NUMBER:
        print_string_slowly("Awesome! our meeting is done now, wish you all the best!!!")
        exit()
    else:
        print_string_slowly("No, that doesn't look right.\
        Remember you can always ask for help")

def current_person():
    current_person_name = ""
    current_person_welcome_note = "Glad to see that Ericsson was one of your choices for the praktik."
    interview_schedule = val.INTERVIEW_SCHEDULE
    for index in range(len(interview_schedule)):
        # Check the day
        if datetime.now().day == int(interview_schedule[index]["day"]):
            start_team_time = interview_schedule[index]["start_time"].split(':')
            start_team_time_in_minutes = hour_to_minutes(start_team_time[0], start_team_time[1])
            end_team_time = interview_schedule[index]["end_time"].split(':')
            end_team_time_in_minutes = hour_to_minutes(end_team_time[0], end_team_time[1])
            # Compare with current time
            current_time = datetime.now()
            current_time_in_minutes = hour_to_minutes(current_time.hour, current_time.minute)
            if (current_time_in_minutes > start_team_time_in_minutes) and (current_time_in_minutes < end_team_time_in_minutes):
                current_person_name = interview_schedule[index]["name"]
                break
    return current_person_name, current_person_welcome_note