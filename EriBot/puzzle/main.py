"""
This is a python file that you need to modify and run to get the secret number.
Basically, you need to write almost 3 lines of code to make it work (Write the code instead of "WRITE YOUR CODE HERE" lines).
This program should read lines from a text file, every line is a number, and find the largest value in that file, which is the secret number!
When you are done with the changes, run the program from the terminal using: python3 main.py
Or using the play button in the right corner.
"""
max_value = 0 # variable to store the max value
with open('../numbers.txt') as my_file:  # Open the file to read from it
    for line in my_file:  # Loop through every line
        # Hint: when checking the line, use int(line) To get the numerical value of that line
        if ("WRITE YOUR CODE HERE"):
            "WRITE YOUR CODE HERE"

# Print the max_value using the print() function
print("WRITE YOUR CODE HERE")

