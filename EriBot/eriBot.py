import lib
import values as val
import click
from PyInquirer import prompt
from examples import custom_style_2
import sys
import pyfiglet

@click.command()


def main():
    lib.print_file_slowly("hold.txt")
    # Progress bar display
    lib.progress_bar()

    # loading Kernel access
    lib.progress_over_iterable_with_colors()

    result = pyfiglet.figlet_format("EriBot Kernel Version 3 . 0 . 1", font="slant")
    print(result)
    person_name, welcome_note = lib.current_person()
    lib.print_string_slowly("Hej " + person_name + ", " + welcome_note)

    # print the intro text
    lib.print_file_slowly("intro_text.txt")

    while 1:
        answers = prompt(val.QUESTIONS, style=custom_style_2)
        if answers["user_option"] == "Yes, I got it":
            lib.print_string_slowly("Great")
            lib.secret_number_found_check()
        elif answers["user_option"] == "Oh too bad, remember you can always ask for help":
            lib.print_string_slowly("Welcome " + person_name + ", " + welcome_note)


if __name__ == "__main__":
    main()
